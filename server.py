from flask import Flask, render_template
from flask_restful import Resource, Api, reqparse

app = Flask(__name__)
api = Api(app)

#set up contact vars
EXISTING_ITEMS = ['abc123']
TOKEN = 'abc123'

# Set up auth
parser = reqparse.RequestParser()
parser.add_argument('Authorization', type=str, required=False, location='headers')


# Create collection endpoint class
class Collection(Resource):

    # Init request parse for incoming json data
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('email', type=str, required=True,
                                    help='No email provided', location='json')
        self.reqparse.add_argument('name', type=str, required=True,
                                    help='No name provided', location='json')
        super(Collection, self).__init__()

    # post method to collection
    def post(self):

        # heaer and auth
        header = parser.parse_args()
        auth = header['Authorization']

        #if there is no auth, then send status of 401 with a message of unauthorized
        if auth is None:
            return {'collection': 'unauthorized'}, 401

        # json args
        args = self.reqparse.parse_args()
        email = args['email']
        name = args['name']

        if auth == 'Bearer ' + TOKEN:
            if email and name:
                if email == '' or email == 'test@example.com' or email == ' ':
                    return {'collection': 'bad request'}, 400

                if name:
                    if name != '' or name != ' ':
                        return {'collection': 'post'}, 201
                else:
                    return {'collection': 'bad request'}, 400
        else:
            return {'collection': 'unauthorized'}, 401


# Create items endpoint class
class Items(Resource):

    def get(self):
        return {'items': 'getting items'}

    def post(self):
        return {'items': 'posting items'}

    def put(self):
        return {'items': 'updating items'}

    def delte(self):
        return {'items': 'deleting items'}


# Create item endpoint class
class Item(Resource):

    def get(self, item_id):
        # Check if the item_id matches an existing item
        if item_id in EXISTING_ITEMS:
            return {'item': item_id}, 403
        else:
            return {'item': item_id}, 404

    def post(self, item_id):
        # check if the item_id matches an existing item
        if item_id in EXISTING_ITEMS:
            return {'item': item_id}, 403
        else:
            return{'itme': item_id}, 404

    def put(self, item_id):
        return {'item': 'putting' + item_id}

    def delete(self, item_id):
        return {'item': 'delte' + item_id}

#create endpoints from classes
api.add_resource(Collection, '/collection')
api.add_resource(Items, '/item/')
api.add_resource(Item, '/item/<item_id>')

# create index view route
@app.route('/')
def index():
    return render_template('/code_challenge.html')


app.run(debug=True)
